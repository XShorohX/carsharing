from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from .models import TariffModel

@admin.register(TariffModel)
class PostAdmin(ModelAdmin):
    list_display = ('tariff_id', 'name', 'cost', 'rent_min_time_per_hour')
    class Meta:
       model = TariffModel