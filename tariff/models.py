from django.db.models.base import Model
from django.db.models.fields import CharField, IntegerField


# Create your models here.
class TariffModel(Model):
    tariff_id = IntegerField(verbose_name='ID', primary_key=True)
    name = CharField(verbose_name = 'Название', max_length=256)
    cost = IntegerField(verbose_name = 'Стоимость')
    rent_min_time_per_hour = IntegerField(verbose_name = 'Минимальное время аренды в часах')

    def __str__(self):
        return str(self.tariff_id)

    class Meta:
        db_table = 'tariff'
        verbose_name = 'Тариф'
        verbose_name_plural = 'Тарифы'
        ordering = ('tariff_id',)