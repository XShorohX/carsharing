from django.apps import AppConfig


class DiscountcardConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'discountcard'
