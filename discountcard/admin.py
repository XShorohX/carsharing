from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from .models import DiscountCardModel

@admin.register(DiscountCardModel)
class PostAdmin(ModelAdmin):
    list_display = ('card_id', 'cardNumber', 'discount', 'card_type', 'registration_date')
    class Meta:
       model = DiscountCardModel

