from django.db.models.base import Model
from django.db.models.fields import CharField, IntegerField, DateTimeField


# Create your models here.
class DiscountCardModel(Model):
    card_id = IntegerField(verbose_name='ID', primary_key=True)
    cardNumber = CharField(verbose_name = 'Номер карты', max_length=16)
    discount = IntegerField(verbose_name = 'Скидка в процентах')
    card_type = IntegerField(verbose_name = 'Тип карты')
    registration_date = DateTimeField(verbose_name = 'Дата регистрации')

    def __str__(self):
        return str(self.card_id)

    class Meta:
        db_table = 'discount_card'
        verbose_name = 'Скидочная карта'
        verbose_name_plural = 'Скидочные карты'
        ordering = ('card_id',)