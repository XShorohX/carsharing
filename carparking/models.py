from django.db.models.base import Model
from django.db.models.fields import CharField, IntegerField


# Create your models here.
class CarParkingModel(Model):
    car_parking_id = IntegerField(verbose_name='ID', primary_key=True)
    address = CharField(verbose_name = 'Адрес', max_length=256)
    income = IntegerField(verbose_name = 'Доход')
    manager_id = IntegerField(verbose_name = 'ID менеджеров')
    client_id = IntegerField(verbose_name = 'ID клиентов')

    def __str__(self):
        return str(self.car_parking_id)

    class Meta:
        db_table = 'car_parking'
        verbose_name = 'Парковка автомобилей'
        verbose_name_plural = 'Парковки автомобилей'
        ordering = ('car_parking_id',)