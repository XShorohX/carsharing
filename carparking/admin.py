from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from .models import CarParkingModel

@admin.register(CarParkingModel)
class PostAdmin(ModelAdmin):
    list_display = ('car_parking_id', 'address', 'income', 'manager_id', 'client_id')
    class Meta:
       model = CarParkingModel
