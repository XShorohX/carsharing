# Generated by Django 3.2.9 on 2021-11-19 10:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CarParkingModel',
            fields=[
                ('car_parking_id', models.IntegerField(primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=256, verbose_name='Адрес')),
                ('income', models.IntegerField(max_length=256, verbose_name='Доход')),
                ('manager_id', models.IntegerField(verbose_name='ID менеджеров')),
                ('client_id', models.IntegerField(verbose_name='ID клиентов')),
            ],
            options={
                'verbose_name': 'Парковка автомобилей',
                'verbose_name_plural': 'Парковки автомобилей',
                'db_table': 'car_parking',
                'ordering': ('car_parking_id',),
            },
        ),
    ]
