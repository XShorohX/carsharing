from django.db.models.base import Model
from django.db.models.fields import CharField, IntegerField


# Create your models here.
class ClientModel(Model):
    client_id = IntegerField(verbose_name='ID', primary_key=True)
    phone_nubmer = CharField(verbose_name = 'Номер телефона', max_length=256)
    name = CharField(verbose_name = 'Фамилия и инициалы', max_length=256)
    discount_card_id = IntegerField(verbose_name = 'ID скидочной карты')
    automobile_id = IntegerField(verbose_name = 'ID автомобиля')

    def __str__(self):
        return str(self.client_id)

    class Meta:
        db_table = 'client'
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        ordering = ('client_id',)