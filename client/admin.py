from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from .models import ClientModel

@admin.register(ClientModel)
class PostAdmin(ModelAdmin):
    list_display = ('client_id', 'phone_nubmer', 'name', 'discount_card_id', 'automobile_id')
    class Meta:
       model = ClientModel
