from django.db.models.base import Model
from django.db.models.fields import IntegerField


# Create your models here.
class TariffAndAutomobileModel(Model):
    id = IntegerField(verbose_name='ID', primary_key=True)
    tariff_id = IntegerField(verbose_name = 'ID тарифа')
    auto_id = IntegerField(verbose_name = 'ID автомобиля')

    def __str__(self):
        return str(self.id)

    class Meta:
        db_table = 'tariff_and_automobile'
        verbose_name = 'Тариф и автомобиль'
        verbose_name_plural = 'Тарифы и автомобили'
        ordering = ('id',)