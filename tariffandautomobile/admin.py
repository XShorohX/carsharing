from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from .models import TariffAndAutomobileModel

@admin.register(TariffAndAutomobileModel)
class PostAdmin(ModelAdmin):
    list_display = ('id', 'tariff_id', 'auto_id')
    class Meta:
       model = TariffAndAutomobileModel