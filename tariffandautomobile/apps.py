from django.apps import AppConfig


class TariffandautomobileConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tariffandautomobile'
