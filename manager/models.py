from django.db.models.base import Model
from django.db.models.fields import CharField, IntegerField


# Create your models here.
class ManagerCardModel(Model):
    manager_id = IntegerField(verbose_name='ID', primary_key=True)
    name = CharField(verbose_name = 'Фамилия и инициалы', max_length=256)
    exp = CharField(verbose_name = 'Опыт работы', max_length=256)
    salary = IntegerField(verbose_name = 'Зарплата')
    specialization = CharField(verbose_name = 'Специализация', max_length=256)
    phone_number = CharField(verbose_name = 'Номер телефона', max_length=256)

    def __str__(self):
        return "{}:{}".format(self.manager_id, self.name)
    class Meta:
        db_table = 'manager'
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'
        ordering = ('manager_id',)
