from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from .models import ManagerCardModel

@admin.register(ManagerCardModel)
class PostAdmin(ModelAdmin):
    list_display = ('manager_id', 'name', 'exp', 'salary', 'specialization', 'phone_number')
    class Meta:
       model = ManagerCardModel