from django.db.models.base import Model
from django.db.models.fields import CharField, IntegerField


# Create your models here.
class AutomobileModel(Model):
    auto_id = IntegerField(verbose_name = 'ID', primary_key=True)
    model = CharField(verbose_name = 'Модель', max_length=256)
    reg_num = CharField(verbose_name = 'Регистрационный номер', max_length=6)
    color = CharField(verbose_name = 'Цвет', max_length=6)

    def __str__(self):
        return str(self.auto_id)

    class Meta:
        db_table = 'automobile'
        verbose_name = 'Автомобиль'
        verbose_name_plural = 'Автомобили'
        ordering = ('auto_id',)