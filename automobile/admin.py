from django.contrib import admin
from django.contrib.admin.options import ModelAdmin

from .models import AutomobileModel

@admin.register(AutomobileModel)
class PostAdmin(ModelAdmin):
    list_display = ('auto_id', 'model', 'reg_num', 'color')
    class Meta:
       model = AutomobileModel

